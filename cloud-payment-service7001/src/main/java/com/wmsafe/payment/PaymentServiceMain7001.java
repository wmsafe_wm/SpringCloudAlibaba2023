package com.wmsafe.payment;

import io.seata.spring.annotation.datasource.EnableAutoDataSourceProxy;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Payment支付服务引导类
 * @author 大白有点菜
 * @className PaymentServiceMain6002
 * @date 2023-03-31
 * @description
 * @since 1.0
 **/
@SpringBootApplication
@MapperScan("com.wmsafe.payment.mapper")
@EnableAutoDataSourceProxy
public class PaymentServiceMain7001 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentServiceMain7001.class, args);
    }
}
