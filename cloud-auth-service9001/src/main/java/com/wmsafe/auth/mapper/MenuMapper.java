package com.wmsafe.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wmsafe.common.entity.SysMenu;


import java.util.List;

public interface MenuMapper extends BaseMapper<SysMenu> {

    List<String> selectPermsByUserId(Long userid);
}
