package com.wmsafe.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wmsafe.common.entity.SysUser;

public interface UserMapper extends BaseMapper<SysUser> {
}
