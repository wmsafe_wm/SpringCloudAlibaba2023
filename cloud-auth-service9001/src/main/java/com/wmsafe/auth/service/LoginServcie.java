package com.wmsafe.auth.service;

import com.wmsafe.common.entity.SysUser;
import com.wmsafe.common.utils.ResponseData;

public interface LoginServcie {
    ResponseData login(SysUser sysUser);

    ResponseData logout(String userId);
}
