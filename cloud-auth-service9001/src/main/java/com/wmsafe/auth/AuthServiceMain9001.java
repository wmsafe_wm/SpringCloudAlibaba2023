package com.wmsafe.auth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.wmsafe.auth.mapper")
public class AuthServiceMain9001 {
    public static void main(String[] args) {
        SpringApplication.run(AuthServiceMain9001.class, args);
    }
}
