package com.wmsafe.auth.controller;


import com.wmsafe.auth.service.LoginServcie;
import com.wmsafe.common.entity.SysUser;
import com.wmsafe.common.utils.ResponseData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class LoginController {

    @Resource
    private LoginServcie loginServcie;

    @PostMapping("/auth/login")
    public ResponseData login(@RequestBody SysUser sysUser){
        return loginServcie.login(sysUser);
    }

    @RequestMapping("/auth/logout/{userId}")
    public ResponseData logout(@PathVariable("userId") String userId){
        return loginServcie.logout(userId);
    }
}
