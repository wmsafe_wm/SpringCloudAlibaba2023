package com.wmsafe.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wmsafe.order.entity.Order;
import org.apache.ibatis.annotations.Param;

/**
 * 订单服务 Dao接口
 * @author 大白有点菜
 * @className OrderDao
 * @date 2023-03-31
 * @description
 * @since 1.0
 **/
//@Mapper //如果不在启动类添加 @MapperScan 注解进行dao包扫描，那么每个Dao接口都需要加上这个 @Mapper 注解
public interface OrderMapper extends BaseMapper<Order> {

    /**
     * 通过订单号获取数据
     * @param orderNum 订单号
     * @return
     */
    Order getByOrderNum(@Param("orderNum") String orderNum);

    /**
     * 通过主键ID获取数据
     * @param id 主键ID
     * @return
     */
    Order getById(@Param("id") Long id);

    /**
     * 新增
     * @param payment 支付服务实体对象
     * @return
     */
    int add(@Param("payment") Order payment);

    /**
     * 通过订单号删除数据
     * @param orderNum 订单号
     * @return
     */
    int deleteByOrderNum(@Param("orderNum") String orderNum);

    /**
     * 通过主键ID删除数据
     * @param id 主键ID
     * @return
     */
    int deleteById(@Param("id") Long id);
}
