package com.wmsafe.order.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.wmsafe.order.annotation.ExcelField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import java.util.Date;

/**
 * 订单服务 实体类
 * @author 大白有点菜
 * @className Order
 * @date 2023-03-31
 * @description
 * @since 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_order")
@ContentRowHeight(16)
@HeadRowHeight(16)
@ColumnWidth(16)
public class Order {

/**
 CREATE TABLE `tb_order` (
 `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
 `order_num` varchar(64) NOT NULL COMMENT '订单号',
 `user_name` varchar(128) DEFAULT NULL COMMENT '用户名',
 `user_phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户手机号',
 `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
 `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
 PRIMARY KEY (`id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
 */

    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)//会自动插入主键
    @ExcelField()
    @ExcelProperty(value = "id", index = 1)
    private Long id;

    /**
     * 订单号
     */
    @NonNull
    @ExcelField()
    @ExcelProperty(value = "orderNum", index = 2)
    private String orderNum;

    /**
     * 用户名
     */
    @NonNull
    @ExcelField()
    @ExcelProperty(value = "userName", index = 3)
    private String userName;

    /**
     * 用户手机号
     */
    @NonNull
    @ExcelField()
    @ExcelProperty(value = "userPhone", index = 4)
    private String userPhone;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")//@JsonFormat 注解是 Jackson 库的注解，用于在序列化和反序列化 JSON 数据时格式化输出日期字段
    @TableField(fill = FieldFill.INSERT)//新增数据时，自动插入当前时间
//@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")/@DateTimeFormat 注解是 Jackson 库的注解，用于在序列化和反序列化 JSON 数据时格式化输入日期字段
    @ExcelField(header = "创建日期",width = 22)
    @ExcelProperty(value = "创建日期", index = 5)
    @ColumnWidth(20)
    private Date createTime;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT_UPDATE)//新增数据时，自动插入当前时间，修改数据时，自动更新为当前时间
//@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ExcelField(header = "修改日期",width = 22) //自定义注解 用于导出报表的 宽度定义
    @ExcelProperty(value = "修改日期", index = 6)
    @ColumnWidth(20)
    private Date updateTime;
}
