package com.wmsafe.order.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelField {
    String header() default "";  // 列标题

    int width() default 16;  // 列宽度

    int height() default 24;  // 行高度
}
