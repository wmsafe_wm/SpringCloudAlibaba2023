package com.wmsafe.order;

import io.seata.spring.annotation.datasource.EnableAutoDataSourceProxy;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Order订单服务引导类
 * @author 大白有点菜
 * @className OrderServiceMain8001
 * @date 2023-04-01
 * @description
 * @since 1.0
 **/
@SpringBootApplication
@MapperScan("com.wmsafe.order.mapper")
@EnableFeignClients
@EnableAutoDataSourceProxy
public class OrderServiceMain8001 {
    public static void main(String[] args) {
        SpringApplication.run(OrderServiceMain8001.class, args);
    }
}
