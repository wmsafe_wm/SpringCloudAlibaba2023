package com.wmsafe.order.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wmsafe.common.utils.ResponseData;
import com.wmsafe.order.entity.Order;
import com.wmsafe.order.feign.*;
import com.wmsafe.order.service.IOrderService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 订单服务 Controller业务类 Feign远程调用
 * @author 大白有点菜
 * @className OrderFeignController
 * @date 2023-04-03
 * @description
 * @since 1.0
 **/
@RestController
@RequestMapping("order")
public class OrderFeignController {
    @Autowired
    private IOrderService orderService;
    @Autowired
    private PaymentFeignService paymentFeignService;

    /**
     * 通过订单号获取数据（application.yml）
     * @param orderNum
     * @return
     */
    @GetMapping("/feign/query/{orderNum}")
    @GlobalTransactional //开启分布式事务
    public ResponseData queryFeign(@PathVariable("orderNum") String orderNum) {

        int db666 = orderService.deleteByOrderNum("db666"); //如果分布式事务没成功 此条记录会被删除

        ResponseData responseData = paymentFeignService.queryFeign(orderNum); //调用远程服务的接口 该接口会抛出运行时异常

        return responseData;
    }

}
