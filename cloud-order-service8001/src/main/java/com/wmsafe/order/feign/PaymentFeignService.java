package com.wmsafe.order.feign;

import com.wmsafe.common.utils.ResponseData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
/**
 * Feign远程调用Payment服务接口（使用application.yml配置文件定义的feign属性）
 * @author 大白有点菜
 * @className PaymentFeignService
 * @date 2023-04-02
 * @description
 * @since 1.0
 **/
@FeignClient(value = "cloud-payment-service")
public interface PaymentFeignService {

    /**
     * 通过订单号获取数据
     * @param orderNum 订单号
     * @return
     */
    @GetMapping("/payment/feign/query/{orderNum}") //注意payment，这个是支付模块的controller的前缀 不加会404
    ResponseData queryFeign(@PathVariable("orderNum") String orderNum);
}
