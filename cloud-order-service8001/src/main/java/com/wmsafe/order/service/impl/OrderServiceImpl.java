package com.wmsafe.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wmsafe.order.entity.Order;
import com.wmsafe.order.mapper.OrderMapper;
import com.wmsafe.order.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
/**
 * 订单服务 Service实现类
 * @author 大白有点菜
 * @className OrderServiceImpl
 * @date 2023-03-31
 * @description
 * @since 1.0
 **/
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {


    private OrderMapper orderMapper;

    /**
     * 写法二：Spring的注解。Setter方式注入对象，官方推荐这种写法。
     * @param orderMapper
     */
    @Autowired
    public void setPaymentDao(OrderMapper orderMapper) {
        this.orderMapper = orderMapper;
    }

    /**
     * 通过订单号获取数据
     * @param orderNum 订单号
     * @return
     */
    @Override
    public Order getByOrderNum(String orderNum) {
        return orderMapper.getByOrderNum(orderNum);
    }

    /**
     * 通过主键ID获取数据
     * @param id 主键ID
     * @return
     */
    @Override
    public Order getById(Long id) {
        return orderMapper.getById(id);
    }

    /**
     * 新增
     * @param order 支付服务实体对象
     * @return
     */
    @Override
    public int add(Order order) {
        Date currentTime = new Date();
        order.setCreateTime(currentTime);
        order.setUpdateTime(currentTime);
        return orderMapper.add(order);
    }

    /**
     * 通过订单号删除数据
     * @param orderNum 订单号
     * @return
     */
    @Override
    public int deleteByOrderNum(String orderNum) {
        return orderMapper.deleteByOrderNum(orderNum);
    }

    /**
     * 通过主键ID删除数据
     * @param id 主键ID
     * @return
     */
    @Override
    public int deleteById(Long id) {
        return orderMapper.deleteById(id);
    }

}
