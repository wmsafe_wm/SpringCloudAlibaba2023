package com.wmsafe.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayServiceMain6008 {
    public static void main(String[] args) {
        SpringApplication.run(GatewayServiceMain6008.class);
    }
}
