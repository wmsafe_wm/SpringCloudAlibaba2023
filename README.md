# SpringCloudAlibaba2023

#### 介绍
2023 个人 SpringCloudAlibaba整合脚手架

#### 软件架构
Java JDK版本：1.8

使用的版本说明：
SpringCloudAlibaba version 2021.0.4.0 / Spring boot version 2.6.14

整合技术栈：
gateway 网关
redis + redisson分布式锁
rocketmq 消息中间件
nacos 服务注册与发现
sentinel 熔断与限流
seata    分布式事务
mysql + mybatis/Plus 数据库持久层
Spring Security + redis 权限框架

#### 安装教程

0.  设置本地系统host文件内容


    192.168.10.3    my-spring-cloud-alibaba-nacos

    192.168.10.3    my-spring-cloud-alibaba-mysql

    192.168.10.3    my-spring-cloud-alibaba-seata

    192.168.10.3    my-spring-cloud-alibaba-redis

    192.168.10.3    my-spring-cloud-alibaba-rocketmq

    192.168.10.3    my-spring-cloud-alibaba-sentinel


其中192.168.10.3你自己对应你系统中的ip我这是都是本地环境所以都是这个ip

示例：
![输入图片说明](%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20230611204047.png)

1.  docker 安装 redis 密码设置123456
2.  安装 mysql 设置root和密码123456
3.  导入seata数据库到mysql
4.  docker 安装nacos2.x版本 密码设置看配置文件
5.  docker 安装rocketmq 和 sentinel 和 seata
6.  mysql导入基础数据库用于测试
7.  启动项目

#### 使用说明

1.  待添加说明

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
